﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Device")]
    public class DeviceController : Controller
    {
        [HttpGet]
        public List<Device> Get()
        {
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = @"Server=192.168.0.20,5000;Database=phr;User ID=sa;Password=Key2Sqlpro1@123*;";

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "Select * from Devices";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            reader = sqlCmd.ExecuteReader();
            Device device = null;
            List<Device> deviceList = new List<Device>();
            while (reader.Read())
            {
                device=new Device(reader.GetValue(1).ToString(), new Sensor(reader.GetValue(2).ToString(), reader.GetValue(3).ToString()));
                deviceList.Add(device);
            }
            myConnection.Close();
            return deviceList;                 

        }
        public class Device
        {
            public string macid;
            public Sensor sensor;
            public Device()
            {
                this.macid = "";
                this.sensor = null;
            }
            public Device(string macid, Sensor sensor)
            {
                this.macid = macid;
                this.sensor = sensor;
            }
            public void read()
            {
                Console.WriteLine("in read;");
            }
            public string getSensorType()
            {
                return sensor.gettype();
            }

            
        }
        public class Sensor
        {
            private string type;
            private string name;
            public Sensor(string type, string name)
            {
                this.type = type;
                this.name = name;
            }
            public string toString()
            {
                return "Name: " + name + ", Type:" + type;
            }
            public string getname()
            {
                return name;
            }
            public string gettype()
            {
                return type;
            }
        }
        //public List<string> Get()
        //{


        //    return new List<string> {
        //        "Data1",
        //        "Data2"
        //    };
        //}
    }
}