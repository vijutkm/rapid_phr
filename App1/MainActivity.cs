﻿using Android.App;
using Android.Widget;
using Android.OS;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using System;
using Android.Content;
using System.Collections.Generic;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Exceptions;
using Plugin.BLE.Android;
using System.Linq;
using Java.Util;
using Android.Util;


namespace App1
{
    [Activity(Label = "PHR")]
    public class MainActivity : Activity
    {
        public static int PERMISSION_REQUEST_COARSE_LOCATION = 456;
        public Button _textboxResults;

        public IBluetoothLE _bluetoothLe;
        public Plugin.BLE.Abstractions.Contracts.IAdapter adapter;


        public static List<IDevice> deviceList = new List<IDevice>();
        public static List<IDevice> deviceList1 = new List<IDevice>();
        public EditText _textboxResults2, edtMacId, edt_macID;
        public TextView txt_sensorName, txt_macID, txt_pairingStatus, txt_systolic, txt_diastolic, txt_heartRate, txt_time, scanneddevice;
        public static ListView listView1;
        public ImageView imageView;
        public Button buttonshowreading, button12,btnPair,btnUnpair;
        public Guid id;
        public List<Device> mlist;

        public IDevice connectedDevice;
        public static String TAG = "PHR-SAMPLE";
        public List<string> items = new List<string>();
        public static List<ViewCollection> LstviewCollection= new List<ViewCollection>();
        public static PulledItemList_Adapter itemAdapter;
        public string message;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
           
            //edt_macID = FindViewById<EditText>(Resource.Id.edt_macID);
            //txt_sensorName = FindViewById<TextView>(Resource.Id.txt_sensorName);
            //txt_macID = FindViewById<TextView>(Resource.Id.txt_macID);
            //txt_pairingStatus = FindViewById<TextView>(Resource.Id.txt_pairingStatus);
            //txt_systolic = FindViewById<TextView>(Resource.Id.txt_systolic);
            //txt_diastolic = FindViewById<TextView>(Resource.Id.txt_diastolic);
            //txt_heartRate = FindViewById<TextView>(Resource.Id.txt_heartRate);
            //txt_time = FindViewById<TextView>(Resource.Id.txt_time);
            listView1 = FindViewById<ListView>(Resource.Id.list);
            btnPair = FindViewById<Button>(Resource.Id.btnPair);
            scanneddevice = FindViewById<TextView>(Resource.Id.scanneddevice);
            //txt_sensorName.Text = "";
            //txt_macID.Text = "";
            //txt_pairingStatus.Text = "";
            //txt_systolic.Text = "";
            //txt_diastolic.Text = "";
            //txt_heartRate.Text = "";
            //txt_time.Text = "";

            StartService(new Intent(this, typeof(ScanService)));
            itemAdapter = new PulledItemList_Adapter(this, LstviewCollection);
            listView1.Adapter = itemAdapter;

            imageView = FindViewById<ImageView>(Resource.Id.demoImageView);
            if (HomeActivity.measureType == "Weight")
            {
                imageView.SetBackgroundResource(Resource.Drawable.sensor_body_weight);
                scanneddevice.Text = scanneddevice.Text + " Weight";
            }
            else if (HomeActivity.measureType == "Pressure")
            {
                imageView.SetImageResource(Resource.Drawable.sensor_blood_pressure);
                scanneddevice.Text = scanneddevice.Text + "Blood Pressure";
            }
            if (HomeActivity.measureType == "Temperature")
            {
                imageView.SetImageResource(Resource.Drawable.sensor_body_temperature);
                scanneddevice.Text = scanneddevice.Text + " Temperature";
            }
            if (HomeActivity.measureType == "Oxygen")
            {
                imageView.SetBackgroundResource(Resource.Drawable.sensor_blood_oxygen);
                scanneddevice.Text = scanneddevice.Text + " Blood Oxygen";
            }
            if (HomeActivity.measureType == "Glucose")
            {
                imageView.SetImageResource(Resource.Drawable.sensor_blood_glucose);
                scanneddevice.Text = scanneddevice.Text + " Blood Glucose";
            }

        }
       
    } 
}



