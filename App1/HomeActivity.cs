﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xam.Plugin.WebView.Abstractions;
using Xam.Plugin.WebView.Abstractions.Delegates;
using Xam.Plugin.WebView.Abstractions.Enumerations;
//using Xam.Plugin.WebView.Droid;
using XHybrid;

namespace App1
{
    [Activity(Label = "PHR", MainLauncher = true)]
    public class HomeActivity : Activity
    {
        public static string measureType;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Home);
            var btn_wt = FindViewById<Button>(Resource.Id.btn_wt);
            btn_wt.Click += Dowt;
            var btn_temp = FindViewById<Button>(Resource.Id.btn_temp);
            btn_temp.Click += Dotemp;
            var btn_Bp = FindViewById<Button>(Resource.Id.btn_Bp);
            btn_Bp.Click += DoBp;
            var btn_oxygen = FindViewById<Button>(Resource.Id.btn_oxygen);
            btn_oxygen.Click += Dooxygen;
            var btn_glucose = FindViewById<Button>(Resource.Id.btn_glucose);
            btn_glucose.Click += Doglucose;

            //HybridSample hs = new HybridSample();
            // FormsWebViewRenderer.Initialize();
            //Xamarin.Forms.Forms.Init(e);



            //private void OnContentLoaded(object sender, EventArgs eventObj)
            //{
            //    System.Diagnostics.Debug.WriteLine(string.Format("DOM Ready: {0}", eventObj.Sender.Source));
            //    eventObj.Sender.InjectJavascript("csharp('Testing');");
            //}
        }
        protected override void OnPause()
        {
             base.OnPause();
            ClearControls();
           
        }
       
        private void ClearControls()
        {
            MainActivity.LstviewCollection.Clear();
            RunOnUiThread(() => {
                if (MainActivity.itemAdapter != null)
                {
                    MainActivity.itemAdapter.NotifyDataSetChanged();
                }
            });
           
           
        }
        public void Dowt(object sender, EventArgs e)
        {
            measureType = "Weight";
            StartActivity(typeof(MainActivity));
         
        }
        public void Dotemp(object sender, EventArgs e)
        {
            measureType = "Temperature";
            StartActivity(typeof(MainActivity));
        }
        public void DoBp(object sender, EventArgs e)
        {
            measureType = "Pressure";
            StartActivity(typeof(MainActivity));
        }
        public void Dooxygen(object sender, EventArgs e)
        {
            measureType = "Oxygen";
            StartActivity(typeof(MainActivity));
        }
        public void Doglucose(object sender, EventArgs e)
        {
            measureType = "Glucose";
            StartActivity(typeof(MainActivity));
        }
    }
}