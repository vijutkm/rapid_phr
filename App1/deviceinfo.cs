﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App1
{
    public class deviceinfo
    {
        private string mac_id;
        private string device_type;
        private string device_name;
        private string status;
        private string reading;
        public static List<deviceinfo> LstDeviceInfo { get; set; }// = new List<deviceinfo>();
        public deviceinfo()
        {
            mac_id = "";
            device_type = "";
        }
        public deviceinfo(string mac_id, string device_type)
        {
            this.device_type = device_type;
            this.mac_id = mac_id; 
        }
        public deviceinfo(string mac_id, string device_name, string device_type="",string status="",string reading="")
        {
            this.device_type = device_type;
            this.mac_id = mac_id;
            this.device_name = device_name;
            this.status = status;
            this.reading = reading;
        }
        public List<deviceinfo> initList()
        {
            LstDeviceInfo = new List<deviceinfo>();
            LstDeviceInfo.Add(new deviceinfo("00000000-0000-0000-0000-001c05fff5b5","Nonin 3230 Bluetooth Pulse Oximeter","","",""));
            LstDeviceInfo.Add(new deviceinfo("00000000-0000-0000-0000-c8b21e0b587f", "Portal 724 Bluetooth Scale", "", "", ""));
            LstDeviceInfo.Add(new deviceinfo("00000000-0000-0000-0000-006019242c62", "Roche Accu-Chek Aviva Connect", "", "", ""));
            LstDeviceInfo.Add(new deviceinfo("00000000-0000-0000-0000-84dd20e53a46", "A&D 651 BLE A&D Bluetooth Smart BP Meter", "", "", ""));
            LstDeviceInfo.Add(new deviceinfo("00000000-0000-0000-0000-8cde523c2d5c", "Portal 724 Bluetooth Scale Model EM-724BT", "", "", ""));
            return LstDeviceInfo;
        }

        public string getDevicename(string id)
        {
            string devicename;
            deviceinfo deviceinfo = LstDeviceInfo.FirstOrDefault(a=>a.mac_id==id);
            if (deviceinfo!=null)
            {
                return deviceinfo.device_name;
            }
            else
            {
                return "";
            }
        }
        public void SetReading(string id, string reading)
        {
            string devicename;
            deviceinfo deviceinfo = LstDeviceInfo.FirstOrDefault(a => a.mac_id == id);

            if (deviceinfo != null)
            {
                deviceinfo.status = reading;
            }


        }

    }
}