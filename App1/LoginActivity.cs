﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;

namespace App1
{
    [Activity(Label = "PHR")]
    public class LoginActivity : Activity
    {
        EditText userid;
        EditText password;
        public static string patientid = "";
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Login);
            //Get email & password values from edit text  
            userid = FindViewById<EditText>(Resource.Id.edt_userName);
            password = FindViewById<EditText>(Resource.Id.edt_password);
            //Trigger click event of Login Button  
            var button = FindViewById<Button>(Resource.Id.btn_login);
            button.Click += DoLogin;
            //StartActivity(typeof(HomeActivity));
        }
        public void DoLogin(object sender, EventArgs e)
        {

            ScanService ss = new ScanService();
            string token;
            string user = ss.CheckUser(userid.Text.ToLower(), password.Text);
            if (user != "" && user != null)
            {
                //token = ss.GetToken(userid.Text.ToLower(), password.Text);
            }
            else
            {
                Toast.MakeText(this, "Wrong credentials found!", ToastLength.Long).Show();
            }

            patientid = userid.Text;
            Toast.MakeText(this, "Login successfully done!", ToastLength.Long).Show();
            StartActivity(typeof(HomeActivity));

        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
            imm.HideSoftInputFromWindow(userid.WindowToken, 0);
            return base.OnTouchEvent(e);
        }

    }
    }
