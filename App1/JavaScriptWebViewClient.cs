﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;

namespace App1
{
    public class JavaScriptWebViewClient : WebViewClient
    {
        private string msg; 
        public JavaScriptWebViewClient(string msg)
        {
            this.msg = msg;
        }
        public override void OnPageFinished(WebView view, string url)
        {
            base.OnPageFinished(view, url);

            string script = string.Format("javascript:loadMsg('{0}');", msg);

            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                view.EvaluateJavascript(script, null);
            }
            else
            {
                view.LoadUrl(script);
            }
        }
    }
}