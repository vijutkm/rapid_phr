﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App1
{
    public class auth
    {

        public int userId { get; set; }
        public int roleId { get; set; }
        public string userName { get; set; }
        public int userTypeId { get; set; }
        public int status { get; set; }
        public string userRole { get; set; }
        public string token { get; set; }

    }
    public class login
    {

        public string username { get; set; }
        public string password { get; set; }

    }
}