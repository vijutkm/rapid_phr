﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Newtonsoft.Json;
using static App1.PulledItemList_Adapter;
using Xamarin.Forms;
using Controls;
using HybridApp;

namespace App1
{
    [Activity(Label = "PHR")]
    public class ReadingActivity : Activity
    {
        Android.Webkit.WebView webView;
        public static string message;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);          
            Reading rd = JsonConvert.DeserializeObject<Reading>(Intent.GetStringExtra("message"));          
            SetContentView(Resource.Layout.Reading);
            webView = FindViewById<Android.Webkit.WebView>(Resource.Id.wv_html_content);
            webView.Settings.JavaScriptEnabled = true;
            webView.SetWebChromeClient(new WebChromeClient());
            webView.Settings.JavaScriptCanOpenWindowsAutomatically = true;
            webView.Settings.JavaScriptEnabled = true;
            string message=UpdateDisplay(rd.vitals, rd.devclass, rd.weight, rd.glucose);
            webView.SetWebViewClient(new JavaScriptWebViewClient(message));
            ////webView.LoadUrl("file:///android_asset/index.html");
            webView.LoadUrl("https://s3.ap-south-1.amazonaws.com/test.tkm/index.html");
            //HybridPage readingPage = new HybridPage(message);

            //   this.webView.InjectJavaScript(
            //string.Format("document.getElementById(\"os\").innerHTML = \"{0}\";", os));

        }
        public string UpdateDisplay(Vitals vitals, string deviceclass, decimal weight, decimal glucose)
        {
            if (deviceclass.Contains("Oxygen"))
            {
                message = "Your Blood Oxygen readings is " + vitals.bloodoxygen + "% and Heart Rate is " + vitals.heartrate + " bpm";
            }
            else if (deviceclass == "Weight")
            {
                message = "Your Weight is " + weight.ToString()+"Kg";
            }
            else if (deviceclass == "Pressure")
            {
                message = "Your Systolic Pressure is " + vitals.systolicpressure + " mmHg and Diastolic Pressure is " + vitals.diastolicpressure + " mmHg and Pulse is " + vitals.pulse + " bpm";
            }
            else if (deviceclass == "Glucose")
            {
                message = "Your Blood Glucose is " + glucose;
            }
            return message;           
        }
    }
}