﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Reflection;
using Newtonsoft.Json;
using HybridApp;

namespace App1
{
    public class PulledItemList_Adapter : BaseAdapter<ViewCollection>
    {
        private Activity context;
        private List<ViewCollection> AllItemList { get; set; }
        public static string mes;

        public List<ViewCollection> _item { get; set; }

        public PulledItemList_Adapter(Activity context, List<ViewCollection> AllItemList)
        {
            this.AllItemList = AllItemList;
            this.context = context;
        }
        public ViewCollection GetItem_bypos(int position)
        {
            return AllItemList[position];
        }
        public override ViewCollection this[int position]
        {
            get { return AllItemList[position]; }
        }

        public override int Count
        {
            get { return AllItemList.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }


        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            Holder holder = null;
            var view = convertView;

            if (view != null)
                holder = view.Tag as Holder;


            if (holder == null)
            {
                holder = new Holder();
                view = context.LayoutInflater.Inflate(Resource.Layout.listview_item_row, null);
                holder.Text = view.FindViewById<TextView>(Resource.Id.FirstText);
                holder.Text2 = view.FindViewById<TextView>(Resource.Id.SecondText);
                holder.Text3 = view.FindViewById<TextView>(Resource.Id.Status);
                holder.TxtReading = view.FindViewById<TextView>(Resource.Id.TxtReading);
                holder.buttonPair = view.FindViewById<Button>(Resource.Id.btnPair);
                holder.buttonUnPair = view.FindViewById<Button>(Resource.Id.btnUnPair);
                holder.buttonReading = view.FindViewById<Button>(Resource.Id.btnReading);
                view.Tag = holder;
            }

            var current_item = AllItemList[position];


            holder.Text.Text = current_item.Id.ToString().Substring(24,12);
            holder.Text2.Text = current_item.Name;
            holder.Text3.Text = current_item.Status;
            holder.TxtReading.Text = current_item.Reading;
           
            holder.buttonPair.Text = "Pair";
            holder.buttonUnPair.Text = "UnPair";
            holder.buttonReading.Text = "Take Reading";
            Device dev = GetDevice(current_item.Id);
            string devclass = "";
            if (dev != null)
            {
                devclass = dev.sensor.type;
            
            holder.buttonPair.Click += (sender, e) =>
            {
                PairDevice(devclass, current_item.Id);
                holder.Text3.Text = "Paired";

            };
            holder.buttonUnPair.Click += (sender, e) =>
            {
                UnPairDevice(devclass, current_item.Id);
               
                holder.Text3.Text = "UnPaired";

            };
            holder.buttonReading.Click += async (sender, e) =>
            {
                Vitals vitals = new Vitals();
                //Task<Vitals> vitals1= ReadData(devclass, current_item.Id);

                string Reading = "";
                decimal glucose = 0;
                decimal weight = 0;
                MainActivity ma = new MainActivity();
                if (devclass == "Oxygen")
                {
                    vitals = await Task.Run(() => OxygenDevice.Read( current_item.Id));
                    if (vitals.bloodoxygen == 0) { return ; }
                }
                else if (devclass == "PortalBOxygen")
                {
                    vitals = await Task.Run(() => OxygenDevice.PortalBORead( current_item.Id));
                    if (vitals.bloodoxygen == 0) { return; }
                }
                else if (devclass == "Weight")
                {
                    vitals = await Task.Run(() => WeightDevice.Read( current_item.Id));
                    if (weight == 0) { return; }
                }
                else if (devclass == "Pressure")
                {
                    vitals = await Task.Run(() => PressureDevice.Read( current_item.Id));
                    if (vitals.diastolicpressure == 0) { return; }
                }
                else if (devclass == "Glucose")
                {
                    glucose = await Task.Run(() => GlucoseDevice.Read( current_item.Id));
                    if (glucose == 0) { return; }
                }
                else if (devclass == "Lung")
                {
                    vitals = await Task.Run(() => LungDevice.Read( current_item.Id));
                }
                else if (devclass == "Pedometer")
                {
                    vitals = await Task.Run(() => PedometerDevice.Read( current_item.Id));
                }
                // Console.WriteLine("MY RESULT: " + Reading);
                ReadingActivity ractiv = new ReadingActivity();
               
                Intent myIntent = new Intent(this.context, typeof(ReadingActivity));
                Reading rd = new Reading();
                rd.vitals = vitals;
                rd.devclass = devclass;
                            
                myIntent.PutExtra("message", JsonConvert.SerializeObject(rd)); //Optional parameters
                this.context.StartActivity(myIntent);

               
                
                //StartActivity(typeof(ReadingActivity));

            };

                
            }
            return view;
        }

        public async Task<Vitals> ReadData(string devclass, string id)
        {
            Vitals vitals = new Vitals();
            try
            {
                string Reading = "";                
                decimal glucose = 0;
                decimal weight = 0;
                MainActivity ma = new MainActivity();
                if (devclass == "Oxygen")
                {
                    vitals = await Task.Run(() => OxygenDevice.Read(id));
                    //if (vitals.bloodoxygen == 0) { return ; }
                }
                else if (devclass == "PortalBOxygen")
                {
                    vitals = await Task.Run(() => OxygenDevice.PortalBORead(id));
                    //if (vitals.bloodoxygen == 0) { return; }
                }
                else if (devclass == "Weight")
                {
                    vitals = await Task.Run(() => WeightDevice.Read(id));
                    //if (weight == 0) { return; }
                }
                else if (devclass == "Pressure")
                {
                    vitals = await Task.Run(() => PressureDevice.Read(id));
                    //if (vitals.diastolicpressure == 0) { return; }
                }
                else if (devclass == "Glucose")
                {
                    glucose = await Task.Run(() => GlucoseDevice.Read(id));
                    //if (glucose == 0) { return; }
                }
                else if (devclass == "Lung")
                {
                    vitals = await Task.Run(() => LungDevice.Read(id));
                }
                else if (devclass == "Pedometer")
                {
                    vitals = await Task.Run(() => PedometerDevice.Read(id));
                }
                return vitals;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return vitals;
            }
        }

        public string UnPairDevice(string devclass, string id)
        {
            try
            {
                if (devclass == "Oxygen" || devclass == "PortalBOxygen")
            {
                OxygenDevice.UnPairDevice(id);
            }
            else if (devclass == "Weight")
            {
                WeightDevice.UnPairDevice(id);
            }
            else if (devclass == "Pressure")
            {
                PressureDevice.UnPairDevice(id);
            }
            else if (devclass == "Glucose")
            {
                GlucoseDevice.UnPairDevice(id);
            }
            else if (devclass == "Lung")
            {
                LungDevice.UnPairDevice(id);
            }
            else if (devclass == "Pedometer")
            {
                PedometerDevice.UnPairDevice(id);
            }
            return "UnPaired";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "UnPaired";
            }
        }

        public string PairDevice(string devclass, string id)
        {
            try
            {
                if (devclass == "Oxygen" || devclass == "PortalBOxygen")
                {
                    OxygenDevice.PairDevice(id);
                }
                else if (devclass == "Weight")
                {
                    WeightDevice.PairDevice(id);
                }
                else if (devclass == "Pressure")
                {
                    PressureDevice.PairDevice(id);
                }
                else if (devclass == "Glucose")
                {
                    GlucoseDevice.PairDevice(id);
                }
                else if (devclass == "Lung")
                {
                    LungDevice.PairDevice(id);
                }
                else if (devclass == "Pedometer")
                {
                    PedometerDevice.PairDevice(id);
                }
                return "Paired";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "Paired";
            }
        }

        private Device GetDevice(string id)
        {
            ScanService scanService = new ScanService();
            Device device =scanService.GetDevice(id);
            return device;
        }

        public class Holder : Java.Lang.Object
        {
            public TextView Text { get; set; }
            public TextView Text2 { get; set; }
            public TextView Text3 { get; set; }
            public TextView Text4 { get; set; }
            public TextView TxtReading { get; set; }
            public Button buttonPair { get; set; }
            public Button buttonUnPair { get; set; }
            public Button buttonReading { get; set; }
        }
        public class Reading
        {
            public string devclass { get; set; }
            public Vitals vitals { get; set; }
            public decimal glucose { get; set; }
            public decimal weight { get; set; }
           
        }
    }

}