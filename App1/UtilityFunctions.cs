﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App1
{
    public class UtilityFunctions
    {
        
        public String convertBytesToHex(byte[] barr, String format, Boolean convert)
        {
            if (barr == null || barr.Length < 1) return "";
            if (format == null || "".Equals(format.Trim())) format = "%02X ";
            StringBuilder b = new StringBuilder();

            for (int i = 0; i < barr.Length - 1; i++)
            {
                if (!convert) b.Append(String.Format(format, barr[i]));
                else
                {
                    b.Append(String.Format(format, barr[i] & 0xFF));
                }
            }
            if (!convert) b.Append(String.Format(format, barr[barr.Length - 1]));
            else
            {
                b.Append(String.Format(format, barr[barr.Length - 1] & 0xFF));
            }
            //b.trimToSize();
            return b.ToString();
        }

        public byte[] hexStringToByteArray(String s)
        {
            int len = s.Length;
            byte[] data = new byte[len / 2];

            //char c = 'F';
                     

            //for (int i = 0; i < len; i += 2)
            //{
            //    int digit1 = Convert.ToInt32(s[i].ToString(), 16)  ;

            //    int digit2 = Convert.ToInt32(s[i].ToString(), 16);

            //    //data[i / 2] = (byte)((Character.digit(s.charAt(i), 16) << 4)
            //    //        + Character.digit(s.charAt(i + 1), 16));
            //    data[i / 2] = (byte)((digit1)
            //           + digit2);
            //}
            return data;
        }

        public static String convertIntToHex(int WeightValue, String format)
        {
            if (WeightValue == 0 || WeightValue.ToString().Length < 1) return "";
            if (format == null || "".Equals(format.Trim())) format = "%02X ";
            StringBuilder b = new StringBuilder();
            string hex = ((WeightValue & 0XFF)).ToString("X2");
            return hex.ToString();
        }
        public decimal WeightValue(int weight4, int weight5)
        {
            string hex11 = convertIntToHex(weight4, null);
            string hex22 = convertIntToHex(weight5, null);
            string hexValue = hex11 + hex22;
            int decValue = int.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
            int ten = 10;
            decimal weight = (decimal)decValue / (decimal)ten;
            return weight;
        }
    }
}