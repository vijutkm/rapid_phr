﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Android;
using Java.Util;
using Android.Util;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    [Service]
    public class ScanService : Android.App.Service
    {
        public string _deviceID;
        public Plugin.BLE.Abstractions.Contracts.IAdapter _adapter;
        public IDevice currDevice;
        public IBluetoothLE _bleDevice;
        public System.Timers.Timer _timer;
        public static List<Device> deviceList;
        public Dictionary<string, string> hash;
        public static int PERMISSION_REQUEST_COARSE_LOCATION = 456;
        public Button _textboxResults;

        public IBluetoothLE _bluetoothLe;
        public Plugin.BLE.Abstractions.Contracts.IAdapter adapter;
     
        public EditText _textboxResults2, edtMacId, edt_macID;
        public TextView txt_sensorName, txt_macID, txt_pairingStatus, txt_systolic, txt_diastolic, txt_heartRate, txt_time;
        public static ListView listView1;
        public Button buttonshowreading, button12, btnPair, btnUnpair;
        public Guid id;
        public List<Device> mlist;

        public IDevice connectedDevice;
        public static String TAG = "PHR-SAMPLE";
        public List<string> items = new List<string>();

        //This will fetch data from the database with devices and fill in the list ,currently this is hardcoded
        public List<Device> initDevice()
        {
            deviceList = new List<Device>();
            Task<List<Device>> task = GetList();// GetList(LoginActivity.patientid);
          
            deviceList = task.Result;

            return deviceList;
        }

        public string GetToken(string userid,string password)
        {
            try
            {
                login login = new login();
                login.username = userid;
                login.password = password;
               
                var jsonRequest = JsonConvert.SerializeObject(login);
                var content = new StringContent(jsonRequest, Encoding.UTF8, "text/json");

                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                string webservice_path = "http://115.111.177.98:8034";// Resources.GetString(Resource.String.webservice_path);
                var _WebApiUrl = string.Format(webservice_path + "/api/auth/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage messge = client.PostAsync(_WebApiUrl, content).Result;
                var Return_authobj = messge.Content.ReadAsStringAsync().Result;

                var auth= JsonConvert.DeserializeObject<auth>(Return_authobj);
                Log.Info(TAG, "auth token="+auth.token);
                return auth.token;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string CheckUser(string userid, string password)
        {
            try
            {
                login login = new login();
                login.username = userid;
                login.password = password;

                var jsonRequest = JsonConvert.SerializeObject(login);
                var content = new StringContent(jsonRequest, Encoding.UTF8, "text/json");

                //StringContent content = new StringContent(JsonConvert.SerializeObject(product), Encoding.UTF8, "application/json");

                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                string webservice_path = "http://115.111.177.98:8034"; //Resources.GetString(Resource.String.webservice_path);
                var _WebApiUrl = string.Format(webservice_path + "/api/auth/user/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (client.PostAsync(_WebApiUrl, content)==null)
                {

                }

                HttpResponseMessage messge = client.PostAsync(_WebApiUrl, content).Result;
                var Return_obj = messge.Content.ReadAsStringAsync().Result;

                //var auth = JsonConvert.DeserializeObject<auth>(Return_authobj);
                Log.Info(TAG, "user id=" + Return_obj);
                return Return_obj;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string getDevicename(string id)
        {
            //Task<List<Device>> task = GetList("1");
            //deviceList = task.Result;
            Device device = deviceList.FirstOrDefault(s => s.macid == id);
            if (device != null)
            {
                return device.sensor.getname();
            }
            else
            {
                return "";
            }
        }

        public Device GetDevice(string id)
        {
            ScanService ss = new ScanService();
            //ss.deviceList = ss.initDevice();
            Device f = deviceList.Where(s => s.macid == id).FirstOrDefault();
            //if (f != null)
            //{
                return f;
            //}

        }
        public List<Device> GetDevices()
        {
            deviceList = new List<Device>();
            Task<List<Device>> task = GetList();

            deviceList = task.Result;

            return deviceList;
        }
        public static IDevice connectedDevice1;

        public override void OnCreate()
        {
            base.OnCreate();
           // deviceList = initDevice();
            Log.Info(TAG, "OnCreate started");
            _bleDevice = CrossBluetoothLE.Current;
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            //DeviceManipulator dm = new DeviceManipulator();

            DeviceManipulator.Scanning(this, null);

            return StartCommandResult.NotSticky;
        }
        

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

        }
        public async Task<List<Device>> GetList()

        {
            ScanService ss = new ScanService();
            deviceList = new List<Device>();
            try
            {
                HttpClient client = new HttpClient();
                string webservice_path = "http://115.111.177.98:8034";//Resources.GetString(Resource.String.webservice_path);
                var _WebApiUrl = string.Format(webservice_path+"/api/device/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage messge = client.GetAsync(_WebApiUrl).Result;
                var Return_DeviceList = messge.Content.ReadAsStringAsync().Result;
                List<Device> DeviceList = JsonConvert.DeserializeObject<List<Device>>(Return_DeviceList);
                foreach (var data in DeviceList)
                {
                    deviceList.Add(data);
                }
              
            }
            catch (Exception ex)
            {

            }
            return deviceList;
        }    

   
    }
}