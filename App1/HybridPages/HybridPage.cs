﻿using System;
using Xamarin.Forms;
using Controls;
using XLabs.Ioc;
using XLabs.Serialization;
//using XLabs.Platform.Device;

namespace HybridApp
{
    public class HybridPage : ContentPage
    {
        private HybridWebView hybrid;
        private string message;
        public HybridPage(string message)
        {
            //var jsonSerializer = Resolver.Resolve<IJsonSerializer>();
            this.hybrid = new HybridWebView();
            //this.hybrid = new HybridWebView(jsonSerializer)
            //{
            //    BackgroundColor = Color.Blue
            //};

            var buttonHeight = 0;// Device.OnPlatform(100, 100, 100);

            //var layout = new RelativeLayout();

            //layout.Children.Add(this.hybrid,
            //    Constraint.Constant(0),
            //    Constraint.Constant(20),
            //    Constraint.RelativeToParent(p => p.Width),
            //    Constraint.RelativeToParent(p => p.Height - buttonHeight));

            //this.Content = layout;
            this.message = message;
            //this.hybrid.InjectJavaScript(
            //  string.Format("document.getElementById(\"message\").innerHTML = \"{0}\";", message));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.hybrid.LoadFinished += OnLoadFinished;
            this.hybrid.LoadFromContent("file:///android_asset/index.html");
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            this.hybrid.LoadFinished -= OnLoadFinished;
        }

        private void OnLoadFinished(object sender, EventArgs args)
        {
            var message = ""; //Device.OnPlatform("iOS", "Droid", "Windows Phone");

           // var device = Resolver.Resolve<IDevice>();

            this.hybrid.InjectJavaScript(
                string.Format("document.getElementById(\"message\").innerHTML = \"{0}\";", message));

            //this.hybrid.CallJsFunction("myFunction", new { DeviceId = device.Id, Manufacturer = device.Manufacturer });
        }
    }
}
