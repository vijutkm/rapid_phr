﻿using System;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using System.Collections.Generic;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Exceptions;
using System.Linq;
using Android.Util;
using Android.Bluetooth;
using System.Threading.Tasks;
using Xam.Plugin.WebView.Abstractions;
using Xam.Plugin.WebView.Abstractions.Enumerations;
using Xam.Plugin.WebView.Abstractions.Delegates;

namespace App1
{
    public static class DeviceManipulator
    {
        public static Plugin.BLE.Abstractions.Contracts.IAdapter _adapter;
        public static IDevice connectedDevice1;
        public static string TAG = "PHR-SAMPLE";
        public static List<IDevice> deviceList = new List<IDevice>();
        public static List<IDevice> deviceList1 = new List<IDevice>();
        public static string ReturnReading = "";
        public static async void Scanning(object sender, EventArgs e)
        {

            _adapter = CrossBluetoothLE.Current.Adapter;
            Dictionary<string, string> List = new Dictionary<string, string>();
            _adapter.ScanMode = Plugin.BLE.Abstractions.Contracts.ScanMode.LowLatency;
            await _adapter.StopScanningForDevicesAsync();
            if (!_adapter.IsScanning)
            {
                Log.Info(TAG, "Not Scanning");
                _adapter.ScanTimeout = 15000;
                _adapter.DeviceDiscovered += (s, a) =>
                {
                    deviceList.Add(a.Device);
                    Log.Info(TAG, "list[i].id=---------" + a.Device.Id + "========================");
                    Log.Info(TAG, "list[i].name=---------" + a.Device.Name + "========================");
                    Log.Info(TAG, "list[i].State=---------" + a.Device.State + "========================");
                };
                await _adapter.StartScanningForDevicesAsync();
                List<string> devlst1 = new List<string>();
                string devicename = "";
                for (int i = 0; i < deviceList.Count; i++)
                {
                    devlst1.Add(deviceList[i].Id + "-" + deviceList[i].Name);

                }
                deviceList1 = (from c in deviceList
                               orderby c.Id
                               select c).GroupBy(g => g.Id).Select(x => x.FirstOrDefault()).ToList();
                ScanService ss = new ScanService();
                ScanService.deviceList = ss.initDevice();
                string devclass = "";
                for (int k = 0; k < deviceList1.Count; k++)
                {

                    devicename = ss.getDevicename(deviceList1[k].Id.ToString()); // NO hardcoding.... 
                    Device dev = ss.GetDevice(deviceList1[k].Id.ToString());

                    if (dev != null)
                    {
                        devclass = dev.sensor.type;
                    }

                    if (devicename != "" && devicename != null && devclass.Contains(HomeActivity.measureType))
                    {
                        var devExits = MainActivity.LstviewCollection.Where(a => a.Id == deviceList1[k].Id.ToString()).FirstOrDefault();
                        if (devExits == null)
                        {
                            MainActivity.LstviewCollection.Add(new ViewCollection
                            {
                                Id = deviceList1[k].Id.ToString(),
                                Name = devicename
                            });
                        }
                    }

                }
                MainActivity.itemAdapter.NotifyDataSetChanged();


                FormsWebView WebView = new FormsWebView()
                {
                    ContentType = WebViewContentType.LocalFile,
                    Source = "file:///android_asset/index.html"
                };
                WebView.OnNavigationStarted += OnNavigationStarted;
               // WebView.OnNavigationCompleted += OnNavigationCompleted;


                //await _adapter.StopScanningForDevicesAsync();
                Log.Info(TAG, " end StartScanningForDevicesAsync");
            }
            else
            {
                Log.Info(TAG, "Is Scanning");
                _adapter.ScanTimeout = 64000;
                _adapter.DeviceDiscovered += (s, a) => deviceList.Add(a.Device);

                for (int i = 0; i < deviceList.Count; i++)
                {
                    Log.Info(TAG, "deviceList[i].iD=---------" + deviceList[i].Id + "========================");
                    Log.Info(TAG, "deviceList[i].Name=---------" + deviceList[i].Name + "========================");
                }

            }
        }
        public static void OnNavigationStarted(object sender, DecisionHandlerDelegate eventObj)
        {
            if (eventObj.Uri == "www.somebadwebsite.com")
                eventObj.Cancel = true;
        }

        //public static void OnNavigationCompleted(object sender, EventHandler eventObj)
        //{
        //    System.Diagnostics.Debug.WriteLine(string.Format("Load Complete: {0}", eventObj.Uri));
        //}



    }
       


    }
           
        
  

        
