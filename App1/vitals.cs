﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App1
{
    public class Vitals
    {
        public decimal bloodglucose { get; set; }
        public float bloodoxygen { get; set; }
        public float heartrate { get; set; }

        public string fluidlevel { get; set; }
        public float pain { get; set; }
        public string peakflow { get; set; }
        public float temperature { get; set; }
        public string temperatureUnit { get; set; }
        public decimal weight { get; set; }
        public float weightUnit { get; set; }
        public float PT_INR { get; set; }
        public float pedometer { get; set; }

        public float fev1 { get; set; }
        public float fev6 { get; set; }
        public float systolicpressure { get; set; }
        public float diastolicpressure { get; set; }
        public float pulse { get; set; }

    }
}
