﻿using System;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Util;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;

namespace App1
{
    public class WeightDevice : Activity
    {
        public static IAdapter _adapter;
        public static IDevice connectedDevice1;
        public static string TAG = "PHR-SAMPLE";
        public static string ReturnReading = "";
        public static decimal Weight = 0;
        public static Vitals vitals = new Vitals();
        public WeightDevice()
        {

        }
        public string macid;
        public Sensor sensor;
        public string getSensorType()
        {
            return sensor.gettype();
        }
        public WeightDevice(string macid, Sensor sensor)
        {
            this.macid = macid;
            this.sensor = sensor;
            // Console.WriteLine("Sensor being set..." + sensor == null ? "Null" : sensor.toString());
        }
        public static async void PairDevice(string edt_macID)
        {
            try
            {
                _adapter = CrossBluetoothLE.Current.Adapter;
                IDevice cd = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);
                if (connectedDevice1.State == DeviceState.Connected)
                {
                    Log.Info(TAG, "connectedDevice.Name=---------" + connectedDevice1.Name + "========================");
                    Log.Info(TAG, "connectedDevice.Id=---------" + connectedDevice1.Id + "========================");
                }

            }
            catch (DeviceConnectionException ex)
            {
                Log.Info(TAG, "DeviceConnectionException=---------" + ex.Message + "========================");
            }
        }

        public static async void UnPairDevice(string edt_macID)
        {
            try
            {
                _adapter = CrossBluetoothLE.Current.Adapter;
                IDevice cd = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);

                if (cd.State == DeviceState.Connected)
                {
                    Log.Info(TAG, "connectedDevice.Name=---------" + cd.Name + "========================");
                    Log.Info(TAG, "connectedDevice.Id=---------" + cd.Id + "========================");
                    Log.Debug(TAG, "Start UnPairing... with: " + cd.Name);
                    await _adapter.DisconnectDeviceAsync(cd);
                    Log.Debug(TAG, "UnPairing finished.");
                }

            }
            catch (System.Exception e)
            {
                Log.Error(TAG, e.Message);
            }
        }

        public static async Task<Vitals> Read(string edt_macID)
        {
            try
            {
                _adapter = CrossBluetoothLE.Current.Adapter;
               // ConnectParameters p=ConnectParameters
                IDevice cd = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);
                var services = await cd.GetServicesAsync();
                int weight5 = 0, weight4 = 0;
                string message = "Set VALUE";
                for (int i = 0; i < services.Count; i++)
                {
                    Log.Info(TAG, "services name=---------" + services[i].Name + "========================");
                    Log.Info(TAG, "services id=---------" + services[i].Id + "========================");
                    var characteristics = await services[i].GetCharacteristicsAsync();

                    for (int j = 0; j < characteristics.Count; j++)
                    {
                        Log.Info(TAG, "characteristics name=---------" + characteristics[j].Name + "========================");
                        Log.Info(TAG, "characteristics id=---------" + characteristics[j].Id + "========================");
                        if (characteristics[j].CanUpdate)
                        {
                            await characteristics[j].StartUpdatesAsync();
                        }

                        characteristics[j].ValueUpdated += (o, args) =>
                        {
                            var bytes1 = args.Characteristic.Value;

                            for (int k = 0; k < bytes1.Length; k++)
                            {
                                weight5 = bytes1[5];
                                weight4 = bytes1[4];
                                Log.Info(TAG, "bytes1 TOSTRING" + bytes1.ToString() + "========================");
                                Log.Info(TAG, "bytes1[" + k + "]" + bytes1[k] + "========================");
                            }
                            UtilityFunctions uf = new UtilityFunctions();
                            Weight = uf.WeightValue(weight4, weight5);
                            ReturnReading = "Weight=" + Weight.ToString();
                            Log.Info(TAG, ReturnReading + "========================");
                            ReturnReading = uf.convertBytesToHex(bytes1, null, true);
                            //byte[] data=uf.hexStringToByteArray(ReturnReading);
                            //Weight = data[5];
                            vitals.weight = Weight;
                        };

                    }

                }
                Log.Info(TAG, "ReturnReading ---> " + ReturnReading);
                return vitals;
            }
            catch (System.Exception e)
            {
                Log.Error(TAG, e.Message);
                return vitals;
            }
        }

    }     
}
