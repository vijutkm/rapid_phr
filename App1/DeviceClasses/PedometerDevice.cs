﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Bluetooth;
using Android.Util;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;

namespace App1
{
       public class PedometerDevice : Device
        {
            public static Plugin.BLE.Abstractions.Contracts.IAdapter _adapter;
            public static IDevice connectedDevice1;
            public static string TAG = "PHR-SAMPLE";
            public static string ReturnReading = "";
            public static Vitals vitals = new Vitals();
        public PedometerDevice()
            {

            }
            public string getSensorType()
            {
                return sensor.gettype();
            }
            public PedometerDevice(string macid, Sensor sensor)
            {
                this.macid = macid;
                this.sensor = sensor;
            }

            public static async void PairDevice(string edt_macID)
            {
                try
                {
                    _adapter = CrossBluetoothLE.Current.Adapter;

                    connectedDevice1 = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);

                    if (connectedDevice1.State == DeviceState.Connected)
                    {
                        Log.Info(TAG, "connectedDevice.Name=---------" + connectedDevice1.Name + "========================");
                        Log.Info(TAG, "connectedDevice.Id=---------" + connectedDevice1.Id + "========================");
                    }
                }
                catch (DeviceConnectionException ex)
                {
                    Log.Info(TAG, "DeviceConnectionException=---------" + ex.Message + "========================");
                }
            }

            public static async void UnPairDevice(string edt_macID)
            {
                try
                {
                    _adapter = CrossBluetoothLE.Current.Adapter;
                    IDevice cd = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);

                    if (cd.State == DeviceState.Connected)
                    {
                        Log.Info(TAG, "connectedDevice.Name=---------" + cd.Name + "========================");
                        Log.Info(TAG, "connectedDevice.Id=---------" + cd.Id + "========================");
                        Log.Debug(TAG, "Start UnPairing... with: " + cd.Name);
                        await _adapter.DisconnectDeviceAsync(cd);
                        Log.Debug(TAG, "UnPairing finished.");
                    }

                }
                catch (System.Exception e)
                {
                    Log.Error(TAG, e.Message);
                }
            }

        public static async Task<Vitals> Read(string edt_macID)
        {
            _adapter = CrossBluetoothLE.Current.Adapter;

            IDevice cd = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);

            var services = await cd.GetServicesAsync();

            string message = "Set VALUE";

            for (int i = 0; i < services.Count; i++)
            {
                Log.Info(TAG, "services name=---------" + services[i].Name + "========================");
                Log.Info(TAG, "services id=---------" + services[i].Id + "========================");
                var characteristics = await services[i].GetCharacteristicsAsync();

                for (int j = 0; j < characteristics.Count; j++)
                {
                    Log.Info(TAG, "characteristics name=---------" + characteristics[j].Name + "========================");
                    Log.Info(TAG, "characteristics id=---------" + characteristics[j].Id + "========================");
                    if (characteristics[j].CanUpdate)
                    {
                        await characteristics[j].StartUpdatesAsync();
                    }

                    characteristics[j].ValueUpdated += (o, args) =>
                    {
                        var bytes1 = args.Characteristic.Value;

                        for (int k = 0; k < bytes1.Length; k++)
                        {
                            vitals.pedometer = bytes1[7];
                          
                            Log.Info(TAG, "bytes1 TOSTRING" + bytes1.ToString() + "========================");
                            Log.Info(TAG, "bytes1[" + k + "]" + bytes1[k] + "========================");
                        }

                        message = "pedometer steps=" + vitals.pedometer;
                        ReturnReading = "pedometer steps=" + vitals.fev1;
                        Log.Info(TAG, message + "========================");
                        Log.Info(TAG, "SET Class variable: ====>" + ReturnReading);

                    };

                }
            }
            Log.Info(TAG, "ReturnReading ---> " + ReturnReading);
            return vitals;
        }
    }
    }
