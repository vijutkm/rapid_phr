﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App1
{
    public class Device
    {
        public string macid;
        public Sensor sensor;
        public Device()
        {
            this.macid = "";
            this.sensor = null;
        }
        public Device(string macid, Sensor sensor)
        {
            this.macid = macid;
            this.sensor = sensor;
        }
       
        public  void read()
        {
            //Console.WriteLine("in read;");
        }
        public string getSensorType()
        {
            return sensor.gettype();
        }

        public  String toString()
        {
            string s;
            if (sensor == null)
            {
                s = null;
            }
            else
            {
                s = sensor.toString();
            }

            return "macid:" + macid + " (sensor info:" + s + ")";
        }
        public string getDevicename(string id)
        {
            string devicename;
            var b = ScanService.deviceList.AsEnumerable().Where(a => a.macid == id).FirstOrDefault();
           
            devicename = b != null ? b.sensor.getname() : "";
            return devicename;

        }
        
    }

}