﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Android.Bluetooth;
using Android.Util;
using Java.Util;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;

namespace App1
{
        public class OxygenDevice : Device
        {
            public static IAdapter _adapter;
            public static IDevice connectedDevice1;
            public static string TAG = "PHR-SAMPLE";
            public static string ReturnReading = "";
            public static Vitals vitals = new Vitals();
        public OxygenDevice()
            {

            }
        public OxygenDevice(string macid, Sensor sensor)
        {
            this.macid = macid;
            this.sensor = sensor;
        }
        public string getSensorType()
            {
                return sensor.gettype();
            }
        
            public static async void PairDevice(string edt_macID)
            {
                try
                {
                    _adapter = CrossBluetoothLE.Current.Adapter;

                    connectedDevice1 = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);

                    if (connectedDevice1.State == DeviceState.Connected)
                    {
                        Log.Info(TAG, "connectedDevice.Name=---------" + connectedDevice1.Name + "========================");
                        Log.Info(TAG, "connectedDevice.Id=---------" + connectedDevice1.Id + "========================");
                    }
                }
                catch (DeviceConnectionException ex)
                {
                    Log.Info(TAG, "DeviceConnectionException=---------" + ex.Message + "========================");
                }
            }

            public static async void UnPairDevice(string edt_macID)
            {
                try
                {
                    _adapter = CrossBluetoothLE.Current.Adapter;
                    IDevice cd = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);

                    if (cd.State == DeviceState.Connected)
                    {
                        Log.Info(TAG, "connectedDevice.Name=---------" + cd.Name + "========================");
                        Log.Info(TAG, "connectedDevice.Id=---------" + cd.Id + "========================");
                        Log.Debug(TAG, "Start UnPairing... with: " + cd.Name);
                        await _adapter.DisconnectDeviceAsync(cd);
                        Log.Debug(TAG, "UnPairing finished.");
                    }

                }
                catch (System.Exception e)
                {
                    Log.Error(TAG, e.Message);
                }
            }

            public static async Task<Vitals> Read(string edt_macID)
            {
            try
            {
                _adapter = CrossBluetoothLE.Current.Adapter;

                IDevice cd = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);

                var services = await cd.GetServicesAsync();

                string message = "Set VALUE";

                for (int i = 0; i < services.Count; i++)
                {
                    Log.Info(TAG, "services name=---------" + services[i].Name + "========================");
                    Log.Info(TAG, "services id=---------" + services[i].Id + "========================");
                    var characteristics = await services[i].GetCharacteristicsAsync();

                    for (int j = 0; j < characteristics.Count; j++)
                    {
                        Log.Info(TAG, "characteristics name=---------" + characteristics[j].Name + "========================");
                        Log.Info(TAG, "characteristics id=---------" + characteristics[j].Id + "========================");
                        if (characteristics[j].CanUpdate)
                        {
                            await characteristics[j].StartUpdatesAsync();
                        }

                        characteristics[j].ValueUpdated += (o, args) =>
                        {
                            var bytes1 = args.Characteristic.Value;

                            for (int k = 0; k < bytes1.Length; k++)
                            {
                                vitals.bloodoxygen= bytes1[7];
                                vitals.heartrate = bytes1[9];
                                Log.Info(TAG, "bytes1 TOSTRING" + bytes1.ToString() + "========================");
                                Log.Info(TAG, "bytes1[" + k + "]" + bytes1[k] + "========================");
                            }

                            message = "Blood Oxygen=" + vitals.bloodoxygen + " Heart Rate=" + vitals.heartrate;
                            ReturnReading = "Blood Oxygen=" + vitals.bloodoxygen + " Heart Rate=" + vitals.heartrate;
                            Log.Info(TAG, message + "========================");
                            Log.Info(TAG, "SET Class variable: ====>" + ReturnReading);

                        };
                    }
                }
                Log.Info(TAG, "ReturnReading ---> " + ReturnReading);
                return vitals;
            }
            catch (System.Exception e)
            {
                Log.Error(TAG, e.Message);
                return vitals;
            }
        }


       
        public static async Task<Vitals> PortalBORead(string edt_macID)
        {
            try
            {
                _adapter = CrossBluetoothLE.Current.Adapter;

                //IDevice cd = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);
                //var services = await connectedDevice1.GetServicesAsync();
                //var uuid = services[i].Id.toString();
                
                var service = await connectedDevice1.GetServiceAsync(Guid.Parse("ba11f08c-5f14-0b0d-1080-007cbe422d51"));
                        Log.Info(TAG, "service Name=---------" + service.Name + "=======================");
                        Log.Info(TAG, "service id=---------" + service.Id + "===========================");

                        //var characteristics = await services[i].GetCharacteristicsAsync();

                        var characteristic = await service.GetCharacteristicAsync(Guid.Parse("0000cd01-0000-1000-8000-00805f9b34fb"));

                var characteristic02 = await service.GetCharacteristicAsync(Guid.Parse("0000cd02-0000-1000-8000-00805f9b34fb"));
                var characteristic03 = await service.GetCharacteristicAsync(Guid.Parse("0000cd03-0000-1000-8000-00805f9b34fb"));
                var characteristic04 = await service.GetCharacteristicAsync(Guid.Parse("0000cd04-0000-1000-8000-00805f9b34fb"));

                await characteristic.StartUpdatesAsync();
                
                await characteristic02.StartUpdatesAsync();
               
                await characteristic03.StartUpdatesAsync();
                characteristic04.ValueUpdated += (o, args) =>
                {
                    var bytes1 = args.Characteristic.Value;

                    for (int k = 0; k < bytes1.Length; k++)
                    {
                        vitals.bloodoxygen = bytes1[3];
                        vitals.heartrate = bytes1[4];
                        Log.Info(TAG, "bytes1 TOSTRING" + bytes1.ToString() + "========================");
                        Log.Info(TAG, "bytes1[" + k + "]" + bytes1[k] + "========================");
                    }

                    string message = "Blood Oxygen=" + vitals.bloodoxygen + " Heart Rate=" + vitals.heartrate;
                    ReturnReading = "Blood Oxygen=" + vitals.bloodoxygen + " Heart Rate=" + vitals.heartrate;
                    Log.Info(TAG, message + "========================");
                    Log.Info(TAG, "SET Class variable: ====>" + ReturnReading);

                };
                await characteristic04.StartUpdatesAsync();
                var characteristic20 = await service.GetCharacteristicAsync(Guid.Parse("0000cd20-0000-1000-8000-00805f9b34fb"));
                        byte[] bytes = getHexBytes("AA5504B10000B5");
                        await characteristic20.WriteAsync(bytes);
                               
                        await characteristic20.StartUpdatesAsync();
                        var resultcharacteristic1 = await service.GetCharacteristicAsync(Guid.Parse("0000cd20-0000-1000-8000-00805f9b34fb"));
                    
                Log.Info(TAG, "ReturnReading ---> " + ReturnReading);
                return vitals;
            }
            catch (System.Exception e)
            {
                Log.Error(TAG, e.Message);
                return vitals;
            }
        }

        public static byte[] getHexBytes(String message)
        {
            int len = message.Length / 2;
            char[] chars = message.ToCharArray();
            String[] hexStr = new String[len];
            byte[] bytes = new byte[len];
            for (int i = 0, j = 0; j < len; i += 2, j++)
            {
                hexStr[j] = "" + chars[i] + chars[i + 1];
                //bytes[j] = (byte)integer.parseInt(hexStr[j], 16);
                bytes[j] = (byte)Convert.ToInt32(hexStr[j], 16);
            }
            return bytes;
        }

    }

    }
