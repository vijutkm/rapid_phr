﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Bluetooth;
using Android.Util;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;


namespace App1
{
        public class TemperatureDevice : Device
        {
            public static Plugin.BLE.Abstractions.Contracts.IAdapter _adapter;
            public static IDevice connectedDevice1;
            public static string TAG = "PHR-SAMPLE";
            public static string ReturnReading = "";
            public TemperatureDevice()
            {

            }
            public string getSensorType()
            {
                return sensor.gettype();
            }
            public TemperatureDevice(string macid, Sensor sensor)
            {
                this.macid = macid;
                this.sensor = sensor;
            }

            public static async void PairDevice(string edt_macID)
            {
                try
                {
                    _adapter = CrossBluetoothLE.Current.Adapter;

                    connectedDevice1 = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);

                    if (connectedDevice1.State == DeviceState.Connected)
                    {
                        Log.Info(TAG, "connectedDevice.Name=---------" + connectedDevice1.Name + "========================");
                        Log.Info(TAG, "connectedDevice.Id=---------" + connectedDevice1.Id + "========================");
                    }
                }
                catch (DeviceConnectionException ex)
                {
                    Log.Info(TAG, "DeviceConnectionException=---------" + ex.Message + "========================");
                }
            }

            public static async void UnPairDevice(string edt_macID)
            {
                try
                {
                    _adapter = CrossBluetoothLE.Current.Adapter;
                    IDevice cd = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);

                    if (cd.State == DeviceState.Connected)
                    {
                        Log.Info(TAG, "connectedDevice.Name=---------" + cd.Name + "========================");
                        Log.Info(TAG, "connectedDevice.Id=---------" + cd.Id + "========================");
                        Log.Debug(TAG, "Start UnPairing... with: " + cd.Name);
                        await _adapter.DisconnectDeviceAsync(cd);
                        Log.Debug(TAG, "UnPairing finished.");
                    }

                }
                catch (System.Exception e)
                {
                    Log.Error(TAG, e.Message);
                }
            }

            public static async Task<string> Read(string edt_macID)
            {

                _adapter = CrossBluetoothLE.Current.Adapter;

                IDevice cd = await _adapter.ConnectToKnownDeviceAsync(Guid.Parse(edt_macID), ConnectParameters.None);

                int glucose = 0;


                var service = await cd.GetServiceAsync(Guid.Parse("00001808-0000-1000-8000-00805f9b34fb"));

                var characteristic = await service.GetCharacteristicAsync(Guid.Parse("00002a18-0000-1000-8000-00805f9b34fb"));

                characteristic.ValueUpdated += (o, args) =>
                {
                    var bytes1 = args.Characteristic.Value;

                    for (int k = 0; k < bytes1.Length; k++)
                    {
                        glucose = bytes1[3];
                        Log.Info(TAG, "bytes1 TOSTRING" + bytes1.ToString() + "========================");
                        Log.Info(TAG, "bytes1[" + k + "]" + bytes1[k] + "========================");
                    }
                    ReturnReading = "Blood Glucose=" + glucose;
                    Log.Info(TAG, ReturnReading + "========================");

                };

                await characteristic.StartUpdatesAsync();

                await characteristic.StartUpdatesAsync();
                string descriptorId = "00002902-0000-1000-8000-00805f9b34fb";
                var descr = await characteristic.GetDescriptorAsync(Guid.Parse(descriptorId));
                var descriptors = await characteristic.GetDescriptorsAsync();
                await descr.WriteAsync(BluetoothGattDescriptor.EnableIndicationValue.ToArray());
                var resultcharacteristic1 = await service.GetCharacteristicAsync(Guid.Parse("00002a35-0000-1000-8000-00805f9b34fb"));

                Log.Info(TAG, "ReturnReading ---> " + ReturnReading);
                return ReturnReading;
            }

        }
    }
