﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App1
{
    public class Sensor
    {
        public string type;
        public string name;
        //public Vitals vitals;
        public Sensor(string type, string name)
        {
            this.type = type;
            this.name = name;
        }
        public string toString()
        {
            return "Name: " + name + ", Type:" + type;
        }
        public string getname()
        {
            return name;
        }
        public string gettype()
        {
            return type;
        }
    }
}